# Version information used on all builds
PRODUCT_BUILD_PROP_OVERRIDES += BUILD_VERSION_TAGS=release-keys USER=android-build BUILD_UTC_DATE=$(shell date +"%s")

# Clear Rom Version
CLEAR_VERSION_MAJOR = 1
CLEAR_VERSION_MINOR = 2

PRODUCT_PROPERTY_OVERRIDES += \
    ro.clear.version=ClearRom-$(CLEAR_VERSION_MAJOR).$(CLEAR_VERSION_MINOR)-$(TARGET_PRODUCT)
